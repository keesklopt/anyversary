import 'package:flutter/material.dart';

import 'package:path_provider/path_provider.dart';
import 'package:intl/intl.dart';

import 'dart:io';
import 'dart:convert';
//import 'package:bubl/bubl.dart';

import 'main.dart';
import 'personform.dart';

void writePersons() async {
  Directory appDocDir = await getApplicationDocumentsDirectory();
  String appDocPath = appDocDir.path;
  print(appDocPath);
  File f = File('$appDocPath/persons.txt');
  String j = json.encode(MyHomePageState.persons);
  print(j);
  f.writeAsString(j);
}

void readPersons() async {
  Directory appDocDir = await getApplicationDocumentsDirectory();
  String appDocPath = appDocDir.path;
  File f = File('$appDocPath/persons.txt');
  String j = await f.readAsString();
  print (j);
  List<dynamic> plist = json.decode(j);

  MyHomePageState.persons = [];
  for (var x in plist) {
    MyHomePageState.persons.add(Person.fromJson(x));
  }
}

class PersonEditPage extends StatefulWidget {
  @override
  _PersonEditState createState() => new _PersonEditState();
}

class Person {
  String name;
  String email;
  String fullname;
  bool isCheck = false;
  DateTime birthdate;

  Person(String name, String email, String fn, DateTime birthdate) {
    this.name = name;
    this.email = email;
    this.fullname = fn;
    this.birthdate = birthdate;
  }

  Person.fromJson(Map<String, dynamic> j)
      : name = j['name'],
        fullname = j['fullname'],
        isCheck = j['checked'],
        birthdate = DateTime.parse(j['birthdate']),
        email = j['email'];

  Map<String, dynamic> toJson() => {
        'name': name,
        'fullname': fullname,
        'checked': isCheck,
        'birthdate': birthdate.toString(),
        'email': email,
      };
}

class PersonListItem extends StatefulWidget {
  final Person person;

  PersonListItem(this.person);

  @override
  _PersonListState createState() =>
      new _PersonListState(this.person);
}

class _PersonListState extends State<StatefulWidget> {
  final Person person;

  _PersonListState(this.person);

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    PersonForm(currentPerson: this.person)));
      },
      child: new Column(children: <Widget>[
        // of padding + divider
        new Padding(
          padding: new EdgeInsets.all(6.0),
          child: new Row(
            // this spreads out the components over this row
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              new Icon(Icons.person),
              // [Expanded,Flexible,Checkbox]
              new Expanded(
                  child: new Text(
                person.name,
                textScaleFactor: 1.8,
                textAlign: TextAlign.left,
              )),
              new Flexible(
                child: new Column(children: [
                  new Text(
                    person.fullname.toString(),
                    textScaleFactor: 0.8,
                    textAlign: TextAlign.left,
                  ),
                  new Text(
                    DateFormat("yyyy-MM-dd kk:mm").format(person.birthdate),
                    textScaleFactor: 0.8,
                    textAlign: TextAlign.right,
                    style: new TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ], crossAxisAlignment: CrossAxisAlignment.start),
              ),
              new Checkbox(
                  value: person.isCheck,
                  onChanged: (bool value) {
                    setState(() {
                      person.isCheck = value;
                      writePersons();
                    });
                  })
            ],
          ),
        ),
        new Divider(
          height: 2.0,
        ),
      ]),
    );
  }
}

class _PersonEditState extends State<StatefulWidget> {
  bool loading = true;

  void _addPerson() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PersonForm(
                currentPerson: Person("", "", "", DateTime.now()))));
     setState(() {
     });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Manage people"),
      ),
      body: new ListView(
        children: MyHomePageState.persons.map((Person person) {
          return PersonListItem(person);
        }).toList(),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _addPerson,
        tooltip: 'Add new person entry',
        child: new Icon(Icons.add),
      ),
    );
  }
}
