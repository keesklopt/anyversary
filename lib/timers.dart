import 'package:flutter/material.dart';

//import 'package:bubl/bubl.dart';
import 'person.dart';
import 'main.dart';

class TimerPage extends StatefulWidget {
  @override
  _TimerState createState() => new _TimerState();
}

class Countdown extends AnimatedWidget {
  Countdown({ Key key, this.animation }) : super(key: key, listenable: animation);
  Animation<int> animation;

  @override
  build(BuildContext context){
    return new Text(
      animation.value.toString(),
      style: new TextStyle(fontSize: 150.0),
    );
  }
}

class TimerListItem extends StatelessWidget {
  final List<Person> persons;

  TimerListItem(this.persons);

  @override
  Widget build(BuildContext context) {
    if (this.persons.length == 0) {
        return new Container(
            child: Center(
                child: new Text(
                    "Please select some users in the people list",
                    style: new TextStyle(fontSize: 20.0),
                ),
            ),
        );

    }

    return new Column(
        children: <Widget>[
            new Padding(
                padding: new EdgeInsets.all(6.0),
                child: new Row(children: [
                new Expanded(
                    child: new Text(
                    persons[0].name,
                    textScaleFactor: 1.8,
                    textAlign: TextAlign.left,
                )),
                new Flexible(
                    child: new Column(children: [
                    new Text(
                    "person.fullname.toString()",
                    textScaleFactor: 0.8,
                    textAlign: TextAlign.left,
                    ),
                    new Text(
                    'DateFormat("yyyy-MM-dd kk:mm").format(person.birthdate)',
                    textScaleFactor: 0.8,
                    textAlign: TextAlign.right,
                    style: new TextStyle(
                        color: Colors.grey,
                    ),
                    ),
                ], crossAxisAlignment: CrossAxisAlignment.start)),
                ]),
            ),
            new Divider(height: 2.0,),
        ]
       );
    }
}

class _TimerState extends State<StatefulWidget> with TickerProviderStateMixin {
  bool loading = true;
  List<Person> persons = [];

  AnimationController _controller;
  static int kStartValue = 4;

  @override
  void initState() {
    super.initState();
    this.persons = [];
    for (Person p in MyHomePageState.persons) { 
        if (p.isCheck) 
            this.persons.add(p);
    } 
    _controller = new AnimationController(
       vsync: this,
       duration: new Duration(seconds: kStartValue),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Elapsed times"),
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.play_arrow),
        onPressed: () => _controller.forward(from: 0.0),
      ),
      body: new Container(
                child: new Center(
                    child: new Countdown(
                        animation: new StepTween(
                        begin: kStartValue,
                        end: 0,
                        ).animate(_controller),
                    ),
                ),
            ),
      );
  }
}
