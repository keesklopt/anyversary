import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

//import 'package:contacts_service/contacts_service.dart';
//import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

import 'package:bubl/bubl.dart';

import 'package:intl/intl.dart';

import 'main.dart';
import 'person.dart';

import 'dart:async';

// Create a Form Widget
class PersonForm extends StatefulWidget {
  final Person currentPerson;

  PersonForm({Key key, this.currentPerson}) : super(key: key);

  @override
  PersonFormState createState() {
    return PersonFormState(this.currentPerson);
  }
}

// Create a corresponding State class. This class will hold the data related to
// the form.
class PersonFormState extends State<PersonForm> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Person currentPerson;

  PersonFormState(this.currentPerson);

  bool isValidPhoneNumber(String input) {
    final RegExp regex = new RegExp(r'^\(\d\d\d\)\d\d\d\-\d\d\d\d$');
    return regex.hasMatch(input);
  }

  bool isValidDob(String dob) {
    if (dob.isEmpty) return true;
    var d = convertToDate(dob.trim());
    return d != null && d.isBefore(new DateTime.now());
  }

  bool isValidEmail(String input) {
    final RegExp regex = new RegExp(
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
    return regex.hasMatch(input);
  }

  DateTime convertToDate(String input) {
    try {
      var d = DateTime.parse(input);
      return d;
    } catch (e) {
      return null;
    }
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    printc(_scaffoldKey.currentState.toString());
    _scaffoldKey.currentState.showSnackBar(
        new SnackBar(backgroundColor: color, content: new Text(message)));
  }

  void _deletePerson() async {
    Future<bool> rusure =
        YesNoDialog(context, "Are you sure ??", "delete person's data..");

    rusure.then((bool answer) {
      if (answer == true) {
        MyHomePageState.persons.remove(this.currentPerson);
        writePersons();
      }
      Navigator.pop(context);
    });
  }

  void _submitForm() {
    final FormState form = _formKey.currentState;

    if (!form.validate()) {
      showMessage('Form is not valid!  Please review and correct.');
    } else {
      form.save(); //This invokes each onSaved event

      printc('De nieuwe speler is opgeslagen ...');
      printc('Speler Naam: ${currentPerson.name}');
      printc('Volledige Naam: ${currentPerson.fullname}');
      //print('Dob: ${currentPerson.dob}');
      printc('Check: ${currentPerson.isCheck}');
      printc('Date: ${currentPerson.birthdate}');
      printc('Email: ${currentPerson.email}');
      //print('Favorite Color: ${currentPerson.favoriteColor}');
      printc('========================================');

      if (MyHomePageState.persons.contains(this.currentPerson) == false)
        MyHomePageState.persons.add(currentPerson);

      writePersons();
      Navigator.pop(context);
    }
  }

  bool isValidName(String name) {
    if (name.isEmpty) return false;
    //printc(MyHomePageState.persons.toString());
    for (Person p in MyHomePageState.persons) {
      if (name == p.name && p != this.currentPerson) return false;
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    //var format = DateFormat("yyyy-MM-dd kk:mm");
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(title: Text("Edit the details")),
      body: new Form(
        key: _formKey,
        autovalidate: false,
        // list o fields :
        child: new ListView(
          // name row
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          children: <Widget>[
            new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.person_outline),
                  hintText: 'Unique Name',
                  labelText: 'Name',
                ),
                //controller: TextEditingController(text: currentPerson.name.toString()),
                initialValue: currentPerson.name.toString(),
                inputFormatters: [new LengthLimitingTextInputFormatter(30)],
                validator: (value) =>
                    isValidName(value) ? null : 'Please enter a unique name',
                onSaved: (val) {
                  //printc(val);
                  currentPerson.name = val;
                }),
            // full name row
            new TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.person),
                hintText: 'First and Last name',
                labelText: 'Full name',
              ),
              initialValue: currentPerson.fullname.toString(),
              inputFormatters: [new LengthLimitingTextInputFormatter(30)],
              //validator: (val) => val.isEmpty ? 'Volledige naam is nodig' : null,
              onSaved: (val) => currentPerson.fullname = val,
            ),
            new SingleChildScrollView(
              child: TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.email),
                  hintText: 'Enter birth/start day',
                  labelText: 'Date',
                ),
                initialValue: DateFormat("yyyy-MM-dd kk:mm")
                    .format(currentPerson.birthdate),
                keyboardType: TextInputType.datetime,
                validator: (val) => isValidDob(val)
                    ? null
                    : 'Format: yyyy-mm-dd (time optional hh:mm)',
                onSaved: (val) =>
                    currentPerson.birthdate = DateTime.parse(val.trim()),
              ),
            ),
            new TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.email),
                hintText: 'Enter a email address',
                labelText: 'Email',
              ),
              initialValue: currentPerson.email.toString(),
              keyboardType: TextInputType.emailAddress,
              //validator: (value) => isValidEmail(value) ? null : 'Please enter a valid email address',
              onSaved: (val) => currentPerson.email = val,
            ),
            new Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomLeft,
                  child: FloatingActionButton(
                    backgroundColor: Colors.red,
                    onPressed: _deletePerson,
                    tooltip: 'Delete person entry',
                    child: new Icon(Icons.delete),
                    heroTag: null,
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: FloatingActionButton(
                    onPressed: _submitForm,
                    tooltip: 'Update new person entry',
                    child: new Icon(Icons.check),
                    heroTag: null,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
