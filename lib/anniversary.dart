import 'package:flutter/material.dart';
//import 'package:path_provider/path_provider.dart';

import 'package:intl/intl.dart';

import 'package:bubl/bubl.dart' as bubl;

//import 'dart:convert';
//import 'dart:io';
import 'dart:async';

import 'main.dart';
//import 'personform.dart';

bubl.EventSequence getEvents({DateTime start}) {
      
      if (start == null) start = DateTime.now();

      List<DateTime> startlist= [];
    
      for (var prsn in MyHomePageState.persons) { 
        if (prsn.isCheck) 
            startlist.add(prsn.birthdate);
      }

      print(startlist);
      startlist.sort();
      print(startlist);

      List<bubl.Interval> li = [];
      // year sequences
      var seq = bubl.RegularSequence(1);
      li.add(bubl.Interval(bubl.yearunit,seq));
      // 10 months
      seq = bubl.RegularSequence(10);
      li.add(bubl.Interval(bubl.monthunit,seq));
      // 100 weeks
      seq = bubl.RegularSequence(100);
      li.add(bubl.Interval(bubl.weekunit,seq));
      //li.add(bubl.Interval(bubl.months,100));
      //li.add(bubl.Interval(bubl.weeks,1000));
      //li.add(bubl.Interval(bubl.days,10000));
      //li.add(bubl.Interval(bubl.minutes,100000));
      //li.add(bubl.Interval(bubl.seconds,10000000));
      //for (var i in [bubl.seconds,bubl.minutes,bubl.hours,bubl.days,bubl.weeks,bubl.tmonths,bubl.tyears])
      //{
        //li.add(bubl.DecimalRange(i));
        //li.add(bubl.RepeatRange(i));
      //}
      //li.add(bubl.RegularRange(bubl.tyears,1));

      for (var i in [bubl.secunit,bubl.minunit,bubl.hourunit,bubl.dayunit,bubl.weekunit,bubl.monthunit,bubl.yearunit])
      {
        var ds = bubl.DecimalSequence();
        var di = bubl.Interval(i,ds);
        li.add(di);

        var rs = bubl.RepetitiveSequence();
        var ri = bubl.Interval(i,rs);
        li.add(ri);

        var evs = bubl.EvenPatternSequence();
        var evi = bubl.Interval(i,evs);
        li.add(evi);

        var ovs = bubl.OddPatternSequence();
        var ovi = bubl.Interval(i,ovs);
        li.add(ovi);
      }
    
      var ri = bubl.RegularSequence(1000000);
      var a1 = bubl.Interval(bubl.secunit,ri);
      li.add(a1);

      ri = bubl.RegularSequence(1000);
      a1 = bubl.Interval(bubl.weekunit,ri);
      li.add(a1);

      var es = bubl.EventSequence(startlist, li);
      es.initEvent(start);

        return es;

    }
class AnniversaryPage extends StatefulWidget {
  @override
  _AnniversaryState createState() => new _AnniversaryState();
}

class AnniversaryItem extends StatelessWidget {
  final bubl.Event evt;

  AnniversaryItem(this.evt);

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: () {

      },
      child: new Padding(
        padding: new EdgeInsets.all(6.0),
        child: new Row(children: [
          new Expanded(
              child: new Text(
              DateFormat("yyyy-MM-dd kk:mm").format(evt.last),
            textScaleFactor: 1.4,
            textAlign: TextAlign.left,
          )),
          new Flexible(
              child: new Column(children: [
            new Text(
              (evt.amount).toString(),
              textScaleFactor: 0.8,
              textAlign: TextAlign.left,
            ),
            new Text(
              evt.unit.name,
              textScaleFactor: 0.8,
              textAlign: TextAlign.right,
              style: new TextStyle(
                color: Colors.grey,
              ),
            ),
          ], crossAxisAlignment: CrossAxisAlignment.start)),
        ]),
      ),
    );
  }
}

class _AnniversaryState extends State<StatefulWidget> {

  bool isPerformingRequest = false;
  List<bubl.Event> events = [];
  ScrollController _scrollController = new ScrollController();


  @override
  void initState() {
    super.initState();

        /*
      DateTime start = DateTime.now();

      List<DateTime> startlist= [];
    
      for (var prsn in MyHomePageState.persons) { 
        if (prsn.isCheck) 
            startlist.add(prsn.birthdate);
      }

      print(startlist);
      startlist.sort();
      print(startlist);

      List<bubl.Interval> li = [];
      // year sequences
      var seq = bubl.RegularSequence(1);
      li.add(bubl.Interval(bubl.yearunit,seq));
      // 10 months
      seq = bubl.RegularSequence(10);
      li.add(bubl.Interval(bubl.monthunit,seq));
      // 100 weeks
      seq = bubl.RegularSequence(100);
      li.add(bubl.Interval(bubl.weekunit,seq));
      //li.add(bubl.Interval(bubl.months,100));
      //li.add(bubl.Interval(bubl.weeks,1000));
      //li.add(bubl.Interval(bubl.days,10000));
      //li.add(bubl.Interval(bubl.minutes,100000));
      //li.add(bubl.Interval(bubl.seconds,10000000));
      //for (var i in [bubl.seconds,bubl.minutes,bubl.hours,bubl.days,bubl.weeks,bubl.tmonths,bubl.tyears])
      //{
        //li.add(bubl.DecimalRange(i));
        //li.add(bubl.RepeatRange(i));
      //}
      //li.add(bubl.RegularRange(bubl.tyears,1));

      for (var i in [bubl.secunit,bubl.minunit,bubl.hourunit,bubl.dayunit,bubl.weekunit,bubl.monthunit,bubl.yearunit])
      {
        var ds = bubl.DecimalSequence();
        var di = bubl.Interval(i,ds);
        li.add(di);

        var rs = bubl.RepetitiveSequence();
        var ri = bubl.Interval(i,rs);
        li.add(ri);
      }
    
      var ri = bubl.RegularSequence(1000000);
      var a1 = bubl.Interval(bubl.secunit,ri);

      var es = bubl.EventSequence(startlist, li);
      es.initEvent(start);
        */

        bubl.EventSequence es = getEvents();

      _getMoreData(es);
      _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        _getMoreData(es);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }


  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isPerformingRequest ? 1.0 : 0.0,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    
    return new Scaffold(
      appBar: AppBar(
        title: Text("Upcoming events"),
      ),
      body: ListView.builder(
        itemCount: events.length + 1,
        itemBuilder: (context, index) {
          if (index == events.length) {
            return _buildProgressIndicator();
          } else {
            return AnniversaryItem(events[index]);
          }
        },
        controller: _scrollController,
      ),
        /*
      floatingActionButton: new FloatingActionButton(
        onPressed: (){},
        tooltip: 'Add new person entry',
        child: new Icon(Icons.add),
      ),
        */
    );
  }

   _getMoreData(bubl.EventSequence es) async {
    if (!isPerformingRequest) {
      setState(() => isPerformingRequest = true);
      List<bubl.Event> newEntries = await _generateList(es);

      setState(() {
        events.addAll(newEntries);
        isPerformingRequest = false;
      });
    }
  }

  Future<List<bubl.Event>> _generateList(bubl.EventSequence es) async {
    var values = new List<bubl.Event>();

    for (var x = 0; x < 100; x++) { 
        bubl.Event iv = es.nextEvent();
        if (iv != null)
            values.add(iv);
    } 

    await new Future.delayed(new Duration(seconds: 1));

    return values;
  }

  Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
    List<bubl.Event> values = snapshot.data;
    return new ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return new Column(
            children: <Widget>[
              new AnniversaryItem(values[index]),
              new Divider(height: 2.0,),
            ],
          );
        },
        controller: _scrollController,
    );
  }
}

