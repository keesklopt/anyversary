import 'package:flutter/material.dart';

//import 'package:path_provider/path_provider.dart';

import 'package:bubl/bubl.dart';

import 'person.dart';
import 'anniversary.dart';
import 'calendar.dart';
import 'timers.dart';

import 'dart:io';
import 'dart:async';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'AnyVersary',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'AnyVersary Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({ Key key, this.title}) : super(key: key);

  final String title;

  @override
  MyHomePageState createState() => MyHomePageState();
}

void exitApp() {
  print("Bye");
  exit(0);
}

List<ButtonData> zelf = [
  ButtonData(
      caption: "People",
      image: 'assets/player.png',
      color: Color(0xFF556677),
      next: DispatchPage(null)),
];

class MyHomePageState extends State<MyHomePage> {
  static final colors = [
    0xFF9e6071,
    0xFF782b73,
    0xFF39399e,
    0xFF495c78,
    0xFF60929e,
    0xFF399e8a,
    0xFF399e46,
    0xFF627849,
    0xFF9e8a39,
    0xFF916f59,
    0xFF913434
  ];

  static var c = 0;

  static List<Person> persons = [];

  final List<ButtonData> list = [
    ButtonData(
        caption: "People",
        image: 'assets/grey.png',
        icondata: Icons.people,
        color: Color(colors[c++]),
        next: PersonEditPage()),
    ButtonData(
        caption: "Events",
        image: 'assets/grey.png',
        icondata: Icons.date_range,
        color: Color(colors[c++]),
        next: CalendarPage()),
    ButtonData(
        caption: "My Anyversaries",
        image: 'assets/grey.png',
        icondata: Icons.cake,
        color: Color(colors[c++]),
        next: AnniversaryPage()),
    ButtonData(
        caption: "Timers",
        image: 'assets/grey.png',
        //icondata: Icons.hourglass_empty,
        icondata: Icons.update,
        color: Color(colors[c++]),
        next: TimerPage()),
  ];

  Future<bool> _exitApp(BuildContext context) {
    return ExitDialog(context) ?? false;
  }

  @override
  void initState() {
    super.initState();
    readPersons();
  }

  @override
  Widget build(BuildContext context) {
    var scaffold = Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Center(
        child: new DispatchPage(list),
      ),
    );

    return WillPopScope(
      onWillPop: () => _exitApp(context),
      child: scaffold,
    );
  }
}
